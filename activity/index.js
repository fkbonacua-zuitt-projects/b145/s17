console.log("Hello World");
// TASK 1: Create a function that will prompt the user for 2 numbers and perform different arithmetic operations based on the total of the two numbers:
// a. If the total of the two numbers is less than 10, add the numbers
// b. If the total of the two numbers is 10 - 20, subtract the numbers
// c. If the total of the two numbers is 21 - 29 multiply the numbers
// d. If the total of the two numbers is greater than or equal to 30, divide the numbers
// e. Use an alert for the total of 10 or greater and a console warning for the total of 9 or less.
function arithmeticOperation(){
	let numA = parseInt(prompt('Provide a number: '));
	let numB = parseInt(prompt('Provide another number: '));
	let total = numA + numB;

	if (total <= 10) {
		console.log("The sum of the two numbers are: " + (numA + numB));
	}	else if (total <= 20) {
		alert("The difference of the two numbers are: " + (numA - numB));
	}	else if (total <= 29) {
		alert("The product of the two numbers are: " + (numA * numB));
	}	else if (total >= 30) {
		alert("The quotient of the two numbers are: " + (numA / numB));
	}	else {
		alert("10 or greater" + (total < 10));
	}
}

function nameAndAge() {
	let name1 = prompt('What is your name? ');
	let age1 = prompt('How old are you? ');
	alert('Hello ' + name1 + ' your age is ' + age1);
}

function checkAgeOfTheUser() {
	let usersAge = parseInt(prompt('Enter your Age: '));
	if (usersAge <= 18) {
		alert('You are now allowed to party!');
	}	else if (usersAge <=21) {
		alert('You are now part of the adult society!');
	}	else if (usersAge <=65) {
		alert('We thank you for contributing to society!');
	}	else {
		alert('Are you sure you\'re not an alien?');
	}
}

// create a function that will determine if the age is too old for preschool
function ageChecker(){
	// we are going to use a try-catch statement instead

	// get the input of the user.
	// the getElementById() will target a component within the document using its ID attribute
	// the "document" parameter describes the HTML document/file where the JS module is linked.
	let userInput = document.getElementById('age').value;
		// alert(userInput);

	// we will now target the element where we will display yhe output of this function.
	let message = document.getElementById('outputDisplay');

	try {
		// lets make sure that the input inserted by the user is NOT equals to a blank string.
		// throw -> this statement examines the input and returns an error.
		console.log(typeof userInput);
		if (userInput === '') throw 'the input is empty';
		// create a conditional statement that will check if the input is NOT a number

		// in order to check if the value is NOT A NUMBER, We will use a isNaN()
		if (isNaN(userInput)) throw 'the input is Not a Number';
		if (userInput <= 7) throw 'the input is good for preschool';
		if (userInput > 7) throw 'too old for preschool';
	}	catch(err) {
		// the "err" is to define the error that will be thrown by the try section. so "err" is caught by the catch statement and a custom error message will be displayed.
		// how are we going to inject a value inside the html container?

		// => using innerHTML property : 
		// syntax: element.innerHTML -> This will allow us to get/set the HTML markup contained within the element
		message.innerHTML = "Age Input: " + err;
	}	finally {
		// this statement here will be executed regardless of the result above.
		console.log('This is from the finally section');
		// lalabas both yung block of code indicated in the finally section including the outcome of the try statement.
	}
}